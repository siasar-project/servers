
DATE=`date +%Y%m%d%H%M%S`
DEST="/home/backup/db"
if [ -z "$1" ]; then
  DATABASES="siasar"
else
  DATABASES=$1
fi
for database in $DATABASES; do
  su postgres -c "pg_dump -O -Fc -d $database" > $DEST/$database-$DATE.dump
  ln -sf $DEST/$database-$DATE.dump $DEST/$database-latest.dump
  #aws s3 cp $DEST/$database-latest.dump s3://siasar/db/
done
#find $DEST -mindepth 1 ! -iname "*010000*.*" -mtime +28 -delete


TOMORROW=`date --date=tomorrow +%d`
echo "$TOMORROW"
if [$TOMORROW -eq "1"]; then
	echo "es el último día del mes"
	find $DEST -mindepth 1 -mtime +28 -delete
fi
